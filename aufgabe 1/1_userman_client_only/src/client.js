/**
 * A user
 */
var User = /** @class */ (function () {
    function User(givenName, familyName, userName) {
        // Shortcut Syntax: 1) Assign userCounter. 2) increment by 1.
        this.id = User.userCounter++;
        this.givenName = givenName;
        this.familyName = familyName;
        /* aufgabe1 */
        this.userName = userName;
        /************/
        // `new Date()` creates a date object using the current date and time.
        this.creationTime = new Date();
    }
    /**
     * When creating a new user, userCounter
     * will be used to determine the next user id.
     */
    User.userCounter = 1;
    return User;
}());
/**
 * A list of users.
 * This class is a simple database, which implements
 * the CRUD operators for a list of users.
 */
var UserList = /** @class */ (function () {
    /**
     * Creates an empty user list.
     */
    function UserList() {
        this.users = [];
    }
    /**
     * Returns the user list.
     * Using a private property and a "getter"-method
     * prevents reassignment of the user list.
     */
    UserList.prototype.getUsers = function () {
        return this.users;
    };
    /**
     * Adds a user to the user list.
     */
    UserList.prototype.addUser = function (user) {
        this.users.push(user);
    };
    /**
     * Deletes the user who is identified by the given id.
     * Returns `true` if the user could be found and deleted.
     */
    UserList.prototype.deleteUser = function (userId) {
        // Search for a user that has the given id using a loop.
        for (var i = 0; i < this.users.length; i++) {
            // Check if the user's id matches the given id.
            if (this.users[i].id == userId) {
                // Remove the user from the array.
                this.users.splice(i, 1);
                // Return true, because the user was found.
                return true;
            }
        }
        // If the loop finishes without returning, the user could not be found.
        return false;
    };
    /**
     * Returns a single user which has the given user id.
     * Important: Do not confuse the user's id with their index in the user list.
     */
    UserList.prototype.getUser = function (userId) {
        // Search for the user by using a loop.
        for (var _i = 0, _a = this.users; _i < _a.length; _i++) {
            var user = _a[_i];
            // Check if the user's id matches the given user id.
            if (userId === user.id) {
                // The id matches, so return the user.
                return user;
            }
        }
        // The user could not be found. Return `null`.
        return null;
    };
    /**
     * Updates the properties of a user who has the given user id.
     * Returns `true`, if the user was found and updated.
     * Returns `false` if no user was found.
     */
    UserList.prototype.editUser = function (userId, givenName, familyName, userName) {
        // Search for the user using a loop
        for (var _i = 0, _a = this.users; _i < _a.length; _i++) {
            var user = _a[_i];
            // Check if the user's id matches the given user id.
            if (user.id === userId) {
                // User found. Update their properties by assigning the given values.
                user.givenName = givenName;
                user.familyName = familyName;
                /* aufgabe1 */
                user.userName = userName;
                /************/
                // Return `true` because a user was found and updated.
                return true;
            }
        }
        // If the loop finishes without returning, no user was found. Return `false`.
        return false;
    };
    return UserList;
}());
/**
 * The user list.
 */
var userList = new UserList();
document.addEventListener("DOMContentLoaded", function () {
    // Add 3 demo entries for test purposes.
    userList.addUser(new User("Samuel", "Schepp", ""));
    userList.addUser(new User("Kevin", "Linne", ""));
    userList.addUser(new User("Peter", "Kneisel", ""));
    renderList();
    // Event handler of the add user button
    document.getElementById("add-user-form").addEventListener("submit", function (event) {
        event.preventDefault();
        var givenNameEl = document.getElementById("add-user-given-name");
        var familyNameEl = document.getElementById("add-user-family-name");
        /* aufgabe1 */
        var userNameEl = document.getElementById("add-user-user-name");
        /************/
        var givenName = givenNameEl.value;
        var familyName = familyNameEl.value;
        /* aufgabe1 */
        var userName = userNameEl.value;
        /************/
        // Check, if any given value is empty.
        /* aufgabe1 */
        if (givenName.trim() === "" || familyName.trim() === "" || userName.trim() === "") {
            addMessage("The given name, family name, or username is empty.");
            return;
        }
        // Don't allow creation of users without given name or family name or user name.
        if (givenName.length == 0 || familyName.length == 0 || userName.length == 0) {
            addMessage("The given name or family name or user name is empty.");
            return;
        }
        // Create the new user.
        var user = new User(givenName, familyName, userName);
        // Add the user to the user list.
        userList.addUser(user);
        addMessage("User added.");
        // Update the html
        renderList();
        // Clear the input fields
        givenNameEl.value = "";
        familyNameEl.value = "";
        userNameEl.value = "";
    });
    // Handler of the modal's 'save' button
    document.getElementById("edit-user-form").addEventListener("submit", function (event) {
        event.preventDefault();
        var idEl = document.getElementById("edit-user-id");
        var givenNameEl = document.getElementById("edit-user-given-name");
        var familyNameEl = document.getElementById("edit-user-family-name");
        var userNameEl = document.getElementById("edit-user-user-name");
        // Read the user's id from the hidden field.
        var userId = Number(idEl.value);
        // .trim() function um leere Zeichen zu entfernen
        var givenName = givenNameEl.value.trim();
        var familyName = familyNameEl.value.trim();
        var userName = userNameEl.value.trim();
        // Don't allow creation of users without given name or family name or username.
        if (givenName.length == 0 || familyName.length == 0 || userName.length == 0) {
            addMessage("The given name or family name or user name is empty.");
            return;
        }
        // Perform the update
        userList.editUser(userId, givenNameEl.value, familyNameEl.value, userNameEl.value);
        addMessage("User updated.");
        /************/
        // Hide the modal window
        bootstrap.Modal.getInstance(document.getElementById("edit-user-modal")).hide();
        // Update the html
        renderList();
    });
});
/**
 * 1) Clears the user table.
 * 2) Adds all users to the table.
 */
function renderList() {
    var userListEl = document.getElementById("user-list");
    // Remove all entries from the table
    userListEl.replaceChildren();
    // Get the user list and other information
    var users = userList.getUsers();
    var numberOfUsers = users.length;
    var totalNameLength = users.reduce(function (total, user) { return total + user.givenName.length + user.familyName.length; }, 0);
    var averageNameLength = totalNameLength / (numberOfUsers * 2);
    var minUserId = Number.POSITIVE_INFINITY;
    var maxUserId = Number.NEGATIVE_INFINITY;
    // Find the smallest and largest user ID
    for (var _i = 0, users_1 = users; _i < users_1.length; _i++) {
        var user = users_1[_i];
        minUserId = Math.min(minUserId, user.id);
        maxUserId = Math.max(maxUserId, user.id);
    }
    //Update the information outside the table
    var userCountEl = document.getElementById("user-count");
    var averageNameLengthEl = document.getElementById("average-name-length");
    var minUserIdEl = document.getElementById("min-user-id");
    var maxUserIdEl = document.getElementById("max-user-id");
    userCountEl.textContent = "".concat(numberOfUsers);
    averageNameLengthEl.textContent = averageNameLength.toFixed(2).toString();
    minUserIdEl.textContent = minUserId.toString();
    maxUserIdEl.textContent = maxUserId.toString();
    var _loop_1 = function (user) {
        // The new table entry
        var tr = document.createElement("tr");
        // ID cell
        var tdId = document.createElement("td");
        tdId.textContent = user.id.toString();
        // First name cell
        var tdGivenName = document.createElement("td");
        tdGivenName.textContent = user.givenName;
        // Last name cell
        var tdFamilyName = document.createElement("td");
        tdFamilyName.textContent = user.familyName;
        // Username cell
        var tdUserName = document.createElement("td");
        tdUserName.textContent = user.userName;
        // Date cell
        var tdDate = document.createElement("td");
        tdDate.textContent = user.creationTime.toLocaleString();
        // Button cell
        var tdButtons = document.createElement("td");
        // Delete button
        var deleteButton = document.createElement("button");
        deleteButton.className = "btn btn-danger";
        deleteButton.addEventListener("click", function () {
            userList.deleteUser(user.id);
            addMessage("User deleted.");
            renderList();
        });
        // Delete button icon
        var deleteButtonIcon = document.createElement("i");
        deleteButtonIcon.className = "fa-solid fa-trash";
        deleteButton.append(deleteButtonIcon);
        //Edit button
        var editButton = document.createElement("button");
        editButton.className = "btn btn-primary ms-3";
        editButton.addEventListener("click", function () {
            showEditModal(user);
        });
        //Edit button icon
        var editButtonIcon = document.createElement("i");
        editButtonIcon.className = "fa-solid fa-pen";
        editButton.append(editButtonIcon);
        // Add the buttons to the button cell
        tdButtons.append(deleteButton, editButton);
        // Add the cells to the row
        tr.append(tdId, tdGivenName, tdFamilyName, tdUserName, tdDate, tdButtons);
        // Add the row to the table
        userListEl.appendChild(tr);
    };
    // Add user rows to the table
    for (var _a = 0, users_2 = users; _a < users_2.length; _a++) {
        var user = users_2[_a];
        _loop_1(user);
    }
}
// Update information about number of users, average name length and IDs.
var numberOfUsers;
document.getElementById("user-count").textContent = numberOfUsers;
var averageNameLength;
document.getElementById("average-name-length").textContent = averageNameLength.toFixed(2);
var minUserId;
document.getElementById("min-user-id").textContent = minUserId;
var maxUserId;
document.getElementById("max-user-id").textContent = maxUserId;
/**
 * 1) Fills the modal window with the given user's data.
 * 2) Opens the modal window.
 */
function showEditModal(user) {
    var idEl = document.getElementById("edit-user-id");
    var givenNameEl = document.getElementById("edit-user-given-name");
    var familyNameEl = document.getElementById("edit-user-family-name");
    var userNameEl = document.getElementById("edit-user-user-name");
    // Write the user's id into the hidden field.
    idEl.value = user.id.toString();
    // Write the user's data into the text fields.
    givenNameEl.value = user.givenName;
    familyNameEl.value = user.familyName;
    userNameEl.value = user.userName;
    // Initialise the modal functionality. Enables the methods `.show()` and `.hide()`.
    var modal = new bootstrap.Modal(document.getElementById("edit-user-modal"));
    // Show the modal window.
    modal.show();
}
/**
 * Creates a new alert message.
 */
function addMessage(message) {
    var messagesEl = document.getElementById('messages');
    // The alert element
    var alertEl = document.createElement('div');
    alertEl.classList.add('alert', 'alert-warning', 'alert-dismissible', 'fade', 'show');
    alertEl.setAttribute('role', 'alert');
    alertEl.textContent = message;
    // Close button
    var buttonEl = document.createElement("button");
    // btn-close changes the button into an 'X' icon.
    buttonEl.className = "btn-close";
    // data-bs-dismiss enables the button to automatically close the alert on click.
    buttonEl.setAttribute("data-bs-dismiss", "alert");
    // Add the close button to the alert.
    alertEl.appendChild(buttonEl);
    // Convert to Bootstrap Alert type
    var alert = new bootstrap.Alert(alertEl);
    // Add message to DOM
    messagesEl.appendChild(alertEl);
    // Auto-remove message after 5 seconds (5000ms)
    setTimeout(function () {
        alert.close();
    }, 5000);
}
//# sourceMappingURL=client.js.map